// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

function loadMovies(movie, resetIndexes) {
  if (resetIndexes) {
    from = 0;
    to = 3;
  }

  // Svuoto lo slider prima di mettere i film nuovi
  // (quelli richiesti attraverso la select)
  slider.innerHTML = "";

  getMovies(movie, function (apiMovies) {
    movies = apiMovies;

    //  sliceMovies(movies, from, to).forEach(function (movie) {
    //     slider.innerHTML +=
    //       '<div class="col-lg-3"><img class="poster" src="' +
    //       movie.image +
    //       '"></div>';
    //   });

    const slices = sliceMovies(movies, from, to);
    //slice elemento su cui itero+array(slices)
    for (const slice of slices) {
      slider.innerHTML += `
      <div class="col-lg-3">
      ${slice.getPosterTag()} 
      </div>`;

      //     '<div class="col-lg-3"> + slice.getPosterTag() + "</div>'
    }
  });
}

let tbody = document.getElementById("tbody");

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

let slider = document.getElementById("slider");
let from = 0;
let to = 3;
let movies = [];

function handleSliderChange(isNext, isBack) {
  if (isNext) {
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else if (isBack) {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(
      e.target.id === "slider-next",
      e.target.id === "slider-back"
    );
  });
});

document.addEventListener("keydown", function (e) {
  e.preventDefault();
  handleSliderChange(e.key === "ArrowRight", e.key === "ArrowLeft");
});

// Individuo la select nel documento
let sel = document.querySelector('select[name="movies"]');

// Inizializzo lo slider con i film di Batman (almeno qualcosa ce trovo)
loadMovies("batman");

sel.addEventListener("change", function (e) {
  // Carico i film indicati dall'utente con la select
  loadMovies(e.target.value, true);
});

//For-in
const o = {
  k1: "valore",
  k2: 10,
  chiappa2: {},
};

// const key conterrà la chiave che di volta in volta viene
// analizzata dal for-in
for (const key in o) {
  // o.key => accedo alla proprietà "key" nell'oggetto "o"
  // NON è corretta perché la costanta key non viene interpellata

  // Accedo alla chiave presente in "key" (la prima volta key conterrà
  //"k1", la seconda "k2", etc...)

  // all'interno dell'oggeto "o"
  console.log(key, o[key]);
}

//ES5
// const keys = Object.keys(o);

// keys.forEach(function (key) {
//   console.log(key, o[key]);
// });

// in (operator)
const o2 = {
  k1: undefined,
  alessandroBorghese: 10,
};

// Con questa sintassi non controllo se esiste la chiave
// bensì il valore ad esso associato

// if (o2.k1) {
//   console.log('k1 esiste');
// }

// if (o2.alessandroBorghese) {
//   console.log('alessandroBorghese esiste');
// }

// Anche una chiave che non esiste in un oggetto restituisce
// "undefined", come spesso accade in Javascript quando si tenta
// di accedere a degli elementi inesistenti

//if(o2.chiaveCheNonEsiste) {

//}

// "In" mi permette di verificare se la chiave presente in "o2"
// esiste, indipendentemente dal valore associato ad essa

if ("k1" in o2) {
  console.log("k1 esiste");
}

if ("alessandroBorghese" in o2) {
  console.log("alessandroBorghese esiste");
}
