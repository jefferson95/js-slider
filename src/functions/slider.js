import { Movie } from "../models/movie";

export function getMovies(s, cb) {
  var xhr = new XMLHttpRequest();
  //"GET" guarda i verbi http su internet
  //"?s=qualcosa" è un queryparam.
  //Essi ci permettono di erogare informazioni aggiuntive sulla richiesta
  xhr.open("GET", "https://fake-movie-database-api.herokuapp.com/api?s=" + s);

  xhr.addEventListener("loadend", function () {
    var data = JSON.parse(xhr.response);

    const users = data.map(u => new User(u.id, u.email.u.username));

    var movies = data.Search.map(function (m) {
      return new Movie(m.imdbID, m.Title, m.Poster);
    });

    cb(movies);
  });

  xhr.send();
}

export function sliceMovies(movies, from, to) {
  var idx = from;
  var goOn = true;
  var ret = [];

  do {
    ret.push(movies[idx]);

    if (idx === to) {
      goOn = false;
    } else if (idx < to || (idx > to && idx < movies.length - 1)) {
      idx++;
    } else {
      idx = 0;
    }
  } while (goOn);

  return ret;
}
